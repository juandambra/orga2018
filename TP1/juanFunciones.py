#columns = ["event"]
#timeDiff = "10 min"
import numpy as np
import pandas as pd
def createSessions(df, columns, timeDiff="10 min"):
    """
        Devuelve un df con el indice original, la sesion ordenada por tiempo y las columnas especificadas que le siguen temporalmente a la sesion
    """
    dfSorted = df.sort_values(by=["person", "timestamp"])[["person", "timestamp"]+ columns]
    nextEvents = dfSorted[columns + ["timestamp"]]                                                 
    nextEvents.index = nextEvents.index-1
    ev = nextEvents.loc[nextEvents.index[-1]]
    nextEvents.loc[nextEvents.index[-1]+1] = ev
    dfSorted = dfSorted.join(nextEvents.loc[0:], rsuffix="_next")
    dfSorted["timestamp_next"] = dfSorted.timestamp_next - dfSorted.timestamp 
    breakIndexes = (dfSorted.groupby("person")["timestamp"].count().cumsum()-1).values
    dfSorted.loc[breakIndexes, ["timestamp_next", "event_next", ]] = np.nan
    serSes = dfSorted.groupby("person")["timestamp_next"].transform(lambda x: x > pd.Timedelta(timeDiff))
    dfSorted["session_number"] = serSes
    dfSorted["session_number"] = dfSorted.groupby("person")["session_number"].transform(lambda x: x.cumsum())
    dfSorted = dfSorted.drop(["timestamp_next", "timestamp", "person"] + columns, axis=1)
    return dfSorted

def generateSessions(df, timeDiff="10 min"):
    """
        Genera sesiones para el dataframe segun el tiempo entre interacciones
    """
    dfSorted = df.sort_values(by=["person", "timestamp"])[["person", "timestamp"]]
    nextEvents = dfSorted[["timestamp"]]                                                 
    nextEvents.index = nextEvents.index-1
    ev = nextEvents.loc[nextEvents.index[-1]]
    nextEvents.loc[nextEvents.index[-1]+1] = ev
    dfSorted = dfSorted.join(nextEvents.loc[0:], rsuffix="_next")
    dfSorted["timestamp_next"] = dfSorted.timestamp_next - dfSorted.timestamp 
    breakIndexes = (dfSorted.groupby("person")["timestamp"].count().cumsum()-1).values
    dfSorted.loc[breakIndexes, ["timestamp_next"]] = np.nan
    serSes = dfSorted.groupby("person")["timestamp_next"].transform(lambda x: x > pd.Timedelta(timeDiff))
    dfSorted["session_number"] = serSes
    dfSorted["session_number"] = dfSorted.groupby("person")["session_number"].transform(lambda x: x.cumsum())
    dfSorted.session_number = dfSorted.groupby(["person"]).session_number.shift(1).fillna(0)
    dfSorted = dfSorted.drop(["timestamp_next", "timestamp", "person"], axis=1)
    return dfSorted["session_number"]

    
def filteredDf(df, event):
    """
        Devuelve un df sin las columnas que tengan todas NaN para determinado evento
    """
    return df.loc[df.event == event].dropna(axis=1, how="all")
                             
def validColumns(df):
    eventosUnicos = df.event.unique()
    dic = dict.fromkeys(eventosUnicos)
    for col in eventosUnicos:
        dic[col] = filteredDf(df, col).columns.drop(["timestamp", "event", "person"]).values
    return dic

def colorsByName():
    filename = "colorCodes"
    lines = []
    with open(filename) as f:
        for line in f:
            vec = line.split()[0:4]
            if len(vec) > 1:
                lines.append(vec)

    dic = {}
    for line in lines:
        try:
            idx = line.index([x for x in line if "#" in x][0])
            name =  " ".join(line[0:idx])
            dic[name] = line[idx]     
        except IndexError:
            pass
        
    return dic

def generateSessionInfo(df):
    sessions = pd.read_csv("sessionsByIdx.csv")
    df = df.join(sessions)
    dfIndexed = df.set_index(["person", "session_number"])

    sessionStats = dfIndexed[['channel', 'new_vs_returning', 'city', 'region', 'country',
            'device_type', 'screen_resolution', 'operating_system_version',
            'browser_version', 'os']].groupby(level=0).ffill().bfill()

    sessionStats.to_csv("session_stats.csv")