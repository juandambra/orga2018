## Consignas
- [ ] ¿Presenta el grupo un listado de "insights" aprendidos sobre los datos en base al análisis realizado? ¿Es interesante?
- [x] ¿Pudieron descubrir features en el campo ***‘model’*** ? ¿Cuales fueron?
- [ ] ¿Identificaron patrones o funnels de usuarios que realizan checkouts/conversiones en Trocafone?
- [ ] ¿Se comportan de forma distinta dependiendo del tipo de dispositivo desde el cual acceden?
- [ ] ¿Se comportan de forma distinta dependiendo del tipo de fuente de tráfico al que pertenecen?
- [x] ¿Realizaron algún análisis sobre búsquedas que realizan los usuarios y las keywords que utilizan apoyándose en algún tipo de visualización?
- [x] ¿Realizaron algún análisis de lugar donde se originan las visitas de los usuarios de Trocafone (a nivel país, regiones más importantes o ciudades más importantes) apoyándose en algún tipo de visualización?
- [x] ¿Pudieron descubrir features jerarquizando información de alguno de los campos (por ejemplo “screen_resolution”)?
- [ ] ¿El análisis realiza un aporte a Trocafone?
