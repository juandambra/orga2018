# TP DATOS 2018

* Juan D'AMBRA
* Tomas BOTALLA
* Carlos BOLLERO
* Joaquin CARBON


# Leer Datos
Para importar set de datos, definir los tipos primero

```python
types = {
 "event" : "category", 
 "person" : "category", 
 "url" : "category", 
 "sku" : "category", 
 "model" : "category", 
 "condition" : "category", 
 "storage" : "category", 
 "color" : "category", 
 "staticpage" : "category", 
 "campaign_source" : "category", 
 "search_engine" : "category", 
 "channel" : "category", 
 "new_vs_returning" : "category", 
 "city" : "category", 
 "region" : "category", 
 "country" : "category", 
 "device_type" : "category", 
 "screen_resolution" : "category", 
 "operating_system_version" : "category", 
 "browser_version" : "category", 
}
```

y despues importar

```python
df = pd.read_csv("fiuba-trocafone-tp1-final-set/events.csv", parse_dates=["timestamp"], dtype=types)```

