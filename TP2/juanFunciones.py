#columns = ["event"]
#timeDiff = "10 min"
import numpy as np
import pandas as pd
def createSessions(df, columns, timeDiff="10 min"):
    """
        Devuelve un df con el indice original, la sesion ordenada por tiempo y las columnas especificadas que le siguen temporalmente a la sesion
    """
    dfSorted = df.sort_values(by=["person", "timestamp"])[["person", "timestamp"]+ columns]
    nextEvents = dfSorted[columns + ["timestamp"]]                                                 
    nextEvents.index = nextEvents.index-1
    ev = nextEvents.loc[nextEvents.index[-1]]
    nextEvents.loc[nextEvents.index[-1]+1] = ev
    dfSorted = dfSorted.join(nextEvents.loc[0:], rsuffix="_next")
    dfSorted["timestamp_next"] = dfSorted.timestamp_next - dfSorted.timestamp 
    breakIndexes = (dfSorted.groupby("person")["timestamp"].count().cumsum()-1).values
    dfSorted.loc[breakIndexes, ["timestamp_next", "event_next", ]] = np.nan
    serSes = dfSorted.groupby("person")["timestamp_next"].transform(lambda x: x > pd.Timedelta(timeDiff))
    dfSorted["session_number"] = serSes
    dfSorted["session_number"] = dfSorted.groupby("person")["session_number"].transform(lambda x: x.cumsum())
    dfSorted = dfSorted.drop(["timestamp_next", "timestamp", "person"] + columns, axis=1)
    return dfSorted

def generateSessions(df, timeDiff="10 min"):
    """
        Genera sesiones para el dataframe segun el tiempo entre interacciones
    """
    dfSorted = df.sort_values(by=["person", "timestamp"])[["person", "timestamp"]]
    nextEvents = dfSorted[["timestamp"]]                                                 
    nextEvents.index = nextEvents.index-1

    dfSorted = dfSorted.reset_index()

    nextEvents = dfSorted[["timestamp"]]                                                 
    nextEvents.index = nextEvents.index-1

    ev = nextEvents.loc[nextEvents.index[-1]]
    nextEvents.loc[nextEvents.index[-1]+1] = ev
    dfSorted = dfSorted.join(nextEvents.loc[0:], rsuffix="_next")
    dfSorted["timestamp_next"] = dfSorted.timestamp_next - dfSorted.timestamp 
    breakIndexes = (dfSorted.groupby("person")["timestamp"].count().cumsum()-1).values

    print("Time Difference {}".format(timeDiff))

    timeDiff = pd.Timedelta(timeDiff)
    dfSorted.loc[breakIndexes, ["timestamp_next"]] = np.nan
    serSes = dfSorted.timestamp_next > timeDiff


    dfSorted["session_number"] = serSes
    dfSorted["session_number"] = dfSorted.groupby("person")["session_number"].transform(lambda x: x.cumsum())
    dfSorted.session_number = dfSorted.groupby(["person"]).session_number.shift(1).fillna(0)
    dfSorted = dfSorted.drop(["timestamp_next", "timestamp", "person"], axis=1).set_index("index")
    return dfSorted

    
def filteredDf(df, event):
    """
        Devuelve un df sin las columnas que tengan todas NaN para determinado evento
    """
    return df.loc[df.event == event].dropna(axis=1, how="all")
                             
def validColumns(df):
    eventosUnicos = df.event.unique()
    dic = dict.fromkeys(eventosUnicos)
    for col in eventosUnicos:
        dic[col] = filteredDf(df, col).columns.drop(["timestamp", "event", "person"]).values
    return dic

def colorsByName():
    filename = "colorCodes"
    lines = []
    with open(filename) as f:
        for line in f:
            vec = line.split()[0:4]
            if len(vec) > 1:
                lines.append(vec)

    dic = {}
    for line in lines:
        try:
            idx = line.index([x for x in line if "#" in x][0])
            name =  " ".join(line[0:idx])
            dic[name] = line[idx]     
        except IndexError:
            pass
        
    return dic

def generateSessionInfo(df):
    sessions = pd.read_csv("sessionsByIdx.csv")
    df = df.join(sessions)
    dfIndexed = df.set_index(["person", "session_number"])

    sessionStats = dfIndexed[['channel', 'new_vs_returning', 'city', 'region', 'country',
            'device_type', 'screen_resolution', 'operating_system_version',
            'browser_version', 'os']].groupby(level=0).ffill().bfill()

    sessionStats.to_csv("session_stats.csv")
    

def splitByPerson(df, amnt, shuffle=False):
    if shuffle:
        df = df.sample(frac=1)
    arr = df["person"].cat.categories
    perm = np.random.permutation(arr)
    # Amnt -1 porque el resto queda para el final, diferencia de 1 a lo sumo
    division = int(arr.size/amnt)
    for i in range(amnt-1):
        divs = perm[i*division:(i+1)*division]
        test = df[df.person.isin(divs) == True].index
        train = df[df.person.isin(divs) == False].index
        yield(train, test)
    
    divs = perm[amnt-1*division:]
    test = df[df.person.isin(divs) == True].index
    train = df[df.person.isin(divs) == False].index
    yield(train, test)
    
def splitByPersonTime(df, amnt):
    arr = df["person"].cat.categories
    perm = np.random.permutation(arr)
    # Amnt -1 porque el resto queda para el final, diferencia de 1 a lo sumo
    division = int(arr.size/(amnt+1))
    for i in range(amnt):
        divs = perm[:(i+1)*division]
        divs2 = perm[(i+1)*division:(i+2)*division]
        train = df[df.person.isin(divs) == True].index
        test = df[df.person.isin(divs2) == True].index
        yield(train, test)
    
def sessionFeatures(df):
    dfs = generateSessions(df, "30 min")
    df = df.join(dfs)
    def sesTime(s):
        return s.max()-s.min()
    dfsessions = df.groupby(["person", "session_number"]).agg({"event":"count","timestamp":sesTime})
    dfSess = dfsessions.reset_index(1).groupby("person").agg({"session_number":"count", "timestamp":["max","min", "sum"]})
    dfSess.columns = dfSess.columns.droplevel(0)
    dfSess = dfSess.rename(columns={"count":"session_count", "max":"time_max", "min":"time_min", "sum":"time_sum"})
    dfSess["time_mean"] = dfSess.time_sum / dfSess.session_count
    dfEv = df.groupby("person").agg({"event":"value_counts"}).unstack(1).fillna(0)
    dfEv.columns = dfEv.columns.droplevel(0)
    dfSess.columns
    dfSessSeconds = dfSess[["time_max", "time_min", "time_mean", "time_sum"]].apply(lambda x: x.dt.total_seconds)
    dfEv = dfEv.join(dfSess.session_count).join(dfSessSeconds)
    dfconv = filteredDf(df, "conversion")
    oneHot = pd.get_dummies(dfconv.drop(columns=["person", "timestamp", "event", "skus"])).join(dfconv.person)
    onehotsum = oneHot.groupby("person").sum()
    dfEv1 = dfEv.join(onehotsum)
    return dfEv1

def generateSubsample(X,labels,size=2000):
    convIdx = labels[labels.label == 1].index.values
    noconvIdx = np.random.choice(labels[labels.label == 0].index, size)
    final = np.concatenate((convIdx, noconvIdx))
    X1 = X.loc[final]
    y1 = labels.loc[final]
    return X1, y1

from sklearn.neighbors import KNeighborsClassifier, NearestNeighbors, NearestCentroid
def subsample2(X,labels,cut=3):
    persY = labels[labels.label == 1].index
    persN = labels[labels.label == 0].index
    dfFeatN = X.loc[persN]
    clf = NearestNeighbors()
    print("fitting neighbors")
    clf.fit(dfFeatN)
    print("generating graph")
    gr = clf.kneighbors_graph()
    gra = gr.toarray()
    print("adding")
    s = pd.DataFrame(gra).sum()
    sp = s[s < cut]
    pNo = dfFeatN.iloc[sp.index].index
    pNo.values
    finalIdx = np.concatenate((pNo.values,persY.values))
    dfFeatTrans = X.loc[finalIdx]
    labelsTrans = labels.loc[finalIdx]
    return dfFeatTrans, labelsTrans

def featureNewVs(df):
    print("generating new vs returning")
    dfv = filteredDf(df, "visited site").sort_values(["person", "timestamp"])
    dfv1 = dfv[["person"]].join(pd.get_dummies(dfv.new_vs_returning)).groupby("person").sum()
    return dfv1

def featureTimeConversion(df):
    pers = df[["person"]].drop_duplicates().set_index("person")
    for evento in ["conversion", "visited site", "checkout"]:
        print("eventos para {}".format(evento))
        dfc = filteredDf(df,evento).set_index("person")
        dfc["week"] = dfc.timestamp.dt.week
        dfc["dayofweek"] = dfc.timestamp.dt.dayofweek
        dfc["dayofyear"] = dfc.timestamp.dt.dayofyear
        dfc["month"] = dfc.timestamp.dt.month

        def countForColumn(df1, col, name):
            df1time = df1.groupby("person")[col].value_counts().unstack(1)
            columns = {}
            for i in df1time.columns:
                columns[i] = "{}_{}_{}".format(evento,name, i)
            df1time = df1time.rename(columns=columns)
            return df1time

        """
        print("Semana")
        df1week = countForColumn(dfc,"week", "Semana")
        print("Mes")
        df1month = countForColumn(dfc, "month", "Mes")
        print("Dia_del_mes")
        df1yearw = countForColumn(dfc, "dayofweek", "Dia_de_semana")
        print("Dia_del_año")
        df1yeary = countForColumn(dfc, "dayofyear", "Dia_del_año")
        """
        
        print("lagged mes")
        dfc1 = dfc.groupby("person")["month"].value_counts().sort_index(ascending=False).groupby("person").cumsum().unstack(1)
        dfc1 = dfc1.fillna(0).sort_index(axis=1, ascending=False).cumsum(axis=1)
        dfc1 = dfc1.add_prefix("{}_from_month_".format(evento))

        print("lagged semana")
        dfc2 = dfc.groupby("person")["week"].value_counts().sort_index(ascending=False).groupby("person").cumsum().unstack(1)
        dfc2 = dfc2.fillna(0).sort_index(axis=1, ascending=False).cumsum(axis=1)
        dfc2 = dfc2.add_prefix("{}_from_week_".format(evento))

        #pers = pers.join(df1week).join(df1month).join(df1yearw).join(df1yeary).join(dfc1).join(dfc2)
        pers = pers.join(dfc1).join(dfc2)
    return pers

## Generate sessions by time difference

def sessionFeatures(df1):
    """  Debe tener una columna sessions """
    try:
        df1["session_number"]
    except:
        print("Debe tener sesiones")
        return
    print("Search Max Time")
    df1time = df[["person", "timestamp","event", "session_number"]]
    df1time = df1time.groupby(["person", "session_number"]).agg({"event":"count","timestamp":["max", "min"]})
    df1time.columns = ["event","max", "min"]
    df1time["total_time"] = df1time["max"] - df1time["min"]
    dfsessions = df1time.reset_index(1)
    print("Max time per user")
    dfsessions.session_number = dfsessions.session_number + 1
    dfSess = dfsessions.groupby("person").agg({"session_number":"max", "total":["max","min", "sum"], "event":["max", "mean"]})
    dfSess.columns = dfSess.columns.droplevel(0)
    dfSess.columns = ["session_count","time_max","time_min","time_sum", "event_max", "event_mean"]   
    dfSess["time_mean"] = dfSess.time_sum / dfSess.session_count
    for i in ["time_max","time_min","time_sum","time_mean"]:
        dfSess[i] = dfSess[i].dt.total_seconds()
    return dfSess
    

def featureEventValues(df):
    print("Value counts Events")
    dfEv = df.groupby("person").agg({"event":"value_counts"}).unstack(1)
    dfEv.columns = dfEv.columns.droplevel(0)
    return dfEv

def featureOneHot(df1):
    dfconv = filteredDf(df1, "conversion")
    print("One hot columns")
    oneHot = pd.get_dummies(dfconv.drop(columns=["person", "timestamp", "event", "skus", "sku","label"])).join(dfconv.person)
    onehotsum = oneHot.groupby("person").sum()
    return onehotsum

def getRandomPerson(df):
    us = np.random.choice(df.person.cat.categories)
    return df[df.person ==us]

#dfp = df.set_index(["person", "session_number"]).sort_index()
#dfs = getRandomPerson(dfp)

def featureSesTiempo(df):
    print("total seconds")
    dfs1 = df.groupby(["person","session_number"]).agg({"timestamp":["max", "min"]})
    #dfs1 = dfs.groupby("session_number").agg({"timestamp":["max", "min"]})
    dfs1.columns = ["max", "min"]
    dfs1["total_time"] = dfs1["max"] - dfs1["min"]
    dfs1["total_time"] = dfs1["total_time"].dt.total_seconds()
    dfs1 = dfs1.drop(columns=["max", "min"]).reset_index(1)
    dic = {}
    values = ["max", "min", "mean", "sum"]
    for col in dfs1.columns:
        dic[col] = values
    dfs1 = dfs1.groupby("person").agg(dic)
    return dfs1

def featureSesEventos(df):
    print("eventsPerSession")
    dic = {}
    values = ["max", "min", "mean", "sum"]
    for cat in df.event.cat.categories:
        dic[cat] = values
    values =df.groupby(["person", "session_number"]).event.value_counts().unstack(2).fillna(0)
    dfVal = values.groupby(["person"]).agg(dic)
    return dfVal

def featureBrand(df):
    dfc = filteredDf(df, "conversion")
    idx = dfc.index
    dfc.columns

    dfc = dfc.drop(columns=["timestamp", "event",])

    s = dfc.model

    models = ["iPhone", "Motorola", "Samsung", "Lenovo", "LG", "Sony","Asus"]

    for model in models:
        dfc.loc[s.str.contains(model),"model_brand"] = model

    dfc.model_brand = dfc.model_brand.fillna("other")

    dfc = dfc.set_index("person")
    dfc1 = dfc.groupby("person").agg({"model_brand" : "max"})
    dfc1 = pd.DataFrame(dfc1)

    dfcc = dfc["condition"].groupby("person").value_counts()

    dfcCond = dfcc.reset_index(1, name="condition_count").sort_values(["person", "condition_count"]).groupby("person").condition.first()

    dfcCond = pd.DataFrame(dfcCond)

    dfc1 = dfc1.join(dfcCond).add_prefix("most_")
    return dfc1

def generateFeatures(df, na=False, oneHot = False, encoded=False, label=None):
    d1 = featureTimeConversion(df)
    d2 = featureSesEventos(df)
    d3 = featureSesTiempo(df)
    d4 = featureEventValues(df)
    d6 =featureNewVs(df)
    dJoin = d2.join(d3).join(d4).join(d6)
    dJoin = dJoin.reset_index().join(d1, on="person").set_index("person")
    if oneHot:
        d5 = featureOneHot(df)
        dJoin.join(d5)
    if na:
        dJoin = dJoin.fillna(0)
    if encoded:
        de = genEncoded(df, label)
        dJoin = dJoin.join(de)
    return dJoin

from sklearn.feature_selection import SelectKBest
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import metrics

def sortScores(X,scoring):
    scores =[]
    for i,score in enumerate(scoring):
        scores.append([score, X.columns[i]])

    return pd.DataFrame(scores).sort_values(by=0, ascending=False)

from sklearn.metrics import roc_auc_score
import matplotlib.pyplot as plt
from sklearn import model_selection
def cvForModels(models, X, Y, cvsplits=5):
    results = []
    names = []
    scoring = 'roc_auc'
    for name, model in models:
        kfold = model_selection.KFold(n_splits=cvsplits, shuffle=True)
        cv_results = model_selection.cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
        results.append(cv_results)
        names.append(name)
        msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
        print(msg)
    # boxplot algorithm comparison
    fig = plt.figure()
    fig.suptitle('Algorithm Comparison')
    plt.boxplot(results)
    ax.set_xticklabels(names)
    plt.show()
    
def featureMaxVisited(df):
    dfv = filteredDf(df, "visited site").drop(columns=["label", "session_number","city"])
    event = "visited_site"
    cols = dfv.columns[3:]
    p = df.person.unique()
    p = pd.DataFrame(p.categories).set_index(0)
    for col in cols:
        print(col)
        dfc = pd.get_dummies(dfv[["person", col]], columns=[col]).groupby("person").sum()
        dfc = dfc.idxmax(axis=1).rename("max_{}_{}".format(event, col))
        p = p.join(dfc)
    return p

def featureMaxCheckout(df):
    event = "checkout"
    dfv = filteredDf(df, event).drop(columns=["label", "session_number"])
    cols = dfv.columns[4:-1]
    p = df.person.unique()
    p = pd.DataFrame(p.categories).set_index(0)
    for col in cols:
        print(col)
        dfc = pd.get_dummies(dfv[["person", col]], columns=[col]).groupby("person").sum()
        dfc = dfc.idxmax(axis=1).rename("max_{}_{}".format(event, col))
        p = p.join(dfc)
    return p

def featureMaxSearched(df):
    event = "searched products"
    dfv = filteredDf(df, event).drop(columns=["label", "session_number", "timestamp", "event", "skus"])

    cols = ["model_brand"]

    dfv = dfv.dropna()
    dfv.model_brand = "Other" 
    models = ["iPhone", "Motorola", "Samsung", "Lenovo", "LG", "Sony","Asus"]
    model_terms = {"iPhone":"iPhone|Iphone|iphone|IPhone|6s", "Motorola":"Motorola|Moto|moto", "Samsung":"Samsung|Galaxy|Sansung|Gran p|J2|j2|J7|j7|J5|j5|s5|S5|S6|s6|S7|s7|s8|S8|A5|A7", "Lenovo":"Lenovo", "LG":"LG|Lg", "Sony":"Sony","Asus":"Asus|asus"}
    for model in models:
        dfv.loc[dfv.search_term.str.contains(model_terms[model]),"model_brand"] = model


    p = df.person.unique()
    p = pd.DataFrame(p.categories).set_index(0)
    for col in cols:
        print(col)
        dfc = pd.get_dummies(dfv[["person", col]], columns=[col]).groupby("person").sum()
        dfc = dfc.idxmax(axis=1).rename("max_{}_{}".format(event, col))
        p = p.join(dfc)
    return p

def featureMaxConversion(df):
    event = "conversion"
    dfc = filteredDf(df, event).drop(columns=["timestamp", "event","sku","skus","label","session_number"])
    dfc.model_brand = "Other" 
    models = ["iPhone", "Motorola", "Samsung", "Lenovo", "LG", "Sony","Asus"]
    model_terms = {"iPhone":"iPhone|Iphone|iphone|IPhone|6s", "Motorola":"Motorola|Moto|moto", "Samsung":"Samsung|Galaxy|Sansung|Gran p|J2|j2|J7|j7|J5|j5|s5|S5|S6|s6|S7|s7|s8|S8|A5|A7", "Lenovo":"Lenovo", "LG":"LG|Lg", "Sony":"Sony","Asus":"Asus|asus"}
    for model in models:
        dfc.loc[dfc.model.str.contains(model_terms[model]),"model_brand"] = model

    dfc = dfc.drop(columns = "model")
    cols = dfc.columns[1:]

    p = df.person.unique()
    p = pd.DataFrame(p.categories).set_index(0)
    for col in cols:
        print(col)
        dfc1 = pd.get_dummies(dfc[["person", col]], columns=[col]).groupby("person").sum()
        dfc1 = dfc1.idxmax(axis=1).rename("max_{}_{}".format(event, col))
        p = p.join(dfc1)
    return p

def encodeFeature(df, featureFn, label):
    import category_encoders as ce
    encoder = ce.TargetEncoder()
    
    dfb = featureFn(df)

    dfb1 = dfb.join(label).reset_index()

    y1 = dfb1.label

    per = dfb1[0]

    dfb1 = dfb1.drop(0, axis=1).drop("label", axis=1)

    dfEnc = encoder.fit_transform(dfb1, y1)

    dfEnc = dfEnc.join(per).set_index(0)
    return dfEnc

def genPersonIndex(df):
    p = df.person.unique()
    p = pd.DataFrame(p.categories).set_index(0)
    return p

def genEncoded(df, label):
    
    p = genPersonIndex(df)
    fns = [featureMaxCheckout, featureMaxConversion, featureMaxSearched, featureMaxVisited]
    for fn in fns:
        dfe = encodeFeature(df, fn, label)
        p = p.join(dfe)
    return p

def plotPred(y_test, y_pred):
    import matplotlib.pyplot as plt
    # Evaluamos el modelo
    roc_auc = roc_auc_score(y_test, y_pred)
    fpr, tpr, thresholds = metrics.roc_curve(y_test, y_pred, pos_label=1)
    plt.figure()
    plt.plot(fpr, tpr, label='Estimator (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.title('ROC')
    plt.xlabel('Tasa Falsos Positivos')
    plt.ylabel('Tasa Verdaderos Positivos')
    plt.legend(loc="lower right")
    plt.savefig('ROC_AUC')
    plt.show()
    
def sortScores(X,scoring):
    scores =[]
    for i,score in enumerate(scoring):
        scores.append([score, X.columns[i]])

    return pd.DataFrame(scores).sort_values(by=0, ascending=False)