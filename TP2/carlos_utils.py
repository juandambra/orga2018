import pandas as pd
import numpy as np
from functools import reduce
from mlxtend.frequent_patterns import apriori, association_rules
from sklearn.preprocessing import scale
from sklearn.model_selection import train_test_split

# READ
def readCSV(path):
    types = {
        "event" : "category", "person" : "category", "url" : "category", "sku" : "category", "model" : "category", 
        "condition" : "category", "storage" : "category", "color" : "category", "staticpage" : "category", 
        "campaign_source" : "category", "search_engine" : "category", "channel" : "category", 
        "new_vs_returning" : "category", "city" : "category", "region" : "category", "country" : "category", 
        "device_type" : "category", "screen_resolution" : "category", "operating_system_version" : "category", 
        "browser_version" : "category","os" : "category","model_brand": "category", "label":"category"
    }
    return pd.read_csv(path, parse_dates=["timestamp"], dtype=types).drop(columns=["Unnamed: 0"])

# FEATURE EXTRACTION
def generateSessions(df, timeDiff=3600):
    """
        Genera sesiones para el dataframe segun el tiempo que pasó del evento anterior.
        Devuelve una Series con el index de df y las sesiones.
    """
    session = 1
    sessions = dict()
    personSessions = [session]
    event_counter = 0
    # Ordeno por persona y timestamp
    sortedDF = df.sort_values(['person','timestamp'])
    # Calculo timedelta en segundos con respecto al elemento anterior
    deltas = sortedDF.timestamp.diff().dt.total_seconds()
    # como el primer elemento no tiene anterior lo deja en NaT
    deltas.values[0] = .0
    # Guardo la primera persona
    prevPerson = sortedDF.person.iloc[0]
    # itero sobre los valores
    # - si el delta es mayor a 1 hora incremento la sesion
    # - si cambio de persona reinicio
    for idx, delta in deltas.iteritems():
        actualPerson = sortedDF.at[idx,'person']
        event_counter += 1
        if (actualPerson != prevPerson):
            if (deltas[idx - 1] < timeDiff):
                if(not f'k_eventos_s{session}' in sessions):
                    sessions[f'k_eventos_s{session}']= {}
                sessions[f'k_eventos_s{session}'][prevPerson] = event_counter-1
            session = 1
            event_counter = 1
            prevPerson = actualPerson
            continue
        if (delta >= timeDiff):
            if(not f'k_eventos_s{session}' in sessions):
                sessions[f'k_eventos_s{session}']= {}
            sessions[f'k_eventos_s{session}'][actualPerson] = event_counter
            session +=1
    return pd.DataFrame.from_dict(sessions).fillna(0)

def assoc_rules(df, min_support=0.03, metric='lift', min_threshold=1.2):
    person_event = df[['person','event']]
    person_event['cant'] = 1
    basket = person_event.groupby(['person','event']).sum().unstack().fillna(0).applymap(lambda x: 1 if x >= 1 else 0)
    basket.columns = basket.columns.droplevel(0)
    basket.columns = [col for col in basket.columns]
    frequent_itemsets = apriori(basket, min_support=min_support, use_colnames=True)
    rules = association_rules(frequent_itemsets, metric=metric, min_threshold=min_threshold)
    rules_for_coversion = rules.loc[['conversion' in x for x in rules.consequents],:]\
                               .sort_values('lift', ascending=False)
    d = dict()
    for row in rules_for_coversion.iterrows():
      d[row[0]] = dict.fromkeys(set().union(*[row[1][0], row[1][1]]),1)
    d = pd.DataFrame.from_dict(d, orient='index').fillna(0)
    d = d.merge(rules_for_coversion[['lift','confidence']], left_index=True, right_index=True)
    cols = ['conversion', 'checkout', 'search engine hit', 'viewed product','staticpage', 
        'generic listing', 'visited site', 'brand listing','ad campaign hit', 'searched products']
    cols2 = cols.copy()
    cols2.append('person')
    merged = basket.reset_index().merge(d, on=cols, how='left')\
                   .sort_values(['person','lift'],ascending=False)\
                   .drop_duplicates(subset=cols2, keep='first')\
                   .sort_index()
    merged['lift'] = scale(merged.lift.values)
    merged.lift.fillna(0, inplace=True)
    merged.confidence.fillna(merged.confidence.mean(), inplace=True)
    merged.rename(columns={'lift':'ass_rules_lift', 'confidence':'ass_rules_confidence'}, inplace=True)
    return merged

def visitFeatures(df):
    visitDF = df[['person','new_vs_returning']].dropna().drop_duplicates(['person','new_vs_returning'])
    visitDF = pd.merge(visitDF[visitDF.new_vs_returning == 'New'], visitDF[visitDF.new_vs_returning == 'Returning'], on='person', how='outer')\
          .rename(columns={'new_vs_returning_x':'new','new_vs_returning_y':'returning'})\
          .replace({'new': {'New': 1, None: 0}, 'returning': {'Returning': 1, None: 0}})
    visitDF = pd.merge(df[['person','event']].drop_duplicates('person'), visitDF, on='person', how='left')\
                .drop(columns=['event'])\
                .fillna(value={'new': 1, 'returning': np.random.choice([0, 1])})
    return visitDF

def lastEvents(df, pivotDate='2018-06-01'):
    pivot = pd.to_datetime(pivotDate)
    last_events = df[['person','event','timestamp']]\
      .sort_values(['person','timestamp'], ascending=False)\
      .drop_duplicates(subset=['person'], keep='first')
    last_events['delta_s_from_last_event'] = last_events.timestamp.apply(lambda x: pd.Timedelta(pivot - x).total_seconds())
    return last_events.drop(columns=['timestamp'])

def eventsCount(df):
    d = df.groupby("person").agg({"event":"value_counts"}).unstack(1).fillna(0)
    d.columns = d.columns.droplevel(0)
    return d.reset_index().rename(columns={'index':'person'})

def modelPopularity(df, col='sku'):
    def getMode(group):
      if(len(group) > 1):
        mode = group.mode()
        if(len(mode) > 1):
          mode = mode.sample()
        return mode
      return group
    total = df[col].value_counts().sum()
    B = total*.8
    C = B + total*.15
    acum = 0
    d = {}
    print('calculando clases...')
    for idx, count in df[col].value_counts().iteritems():
      acum += count
      if(acum > C):
        d[idx] = 0
      elif(acum > B):
        d[idx] = 1
      else:
        d[idx] = 2
    print('armando df de clases...')
    d = pd.DataFrame.from_dict(d, orient='index', columns=['class']).reset_index().rename(columns={'index':'sku'})
    print('calculando sku moda por persona...')
    g = {}
    grouped = df[['person','sku']].dropna().groupby('person', as_index=False)
    for person, skus in grouped:
        mode = skus.sku.mode()
        if(len(mode) > 0):
            if(len(mode) > 1):
              mode = mode.sample()
            g[person] = mode[0]
    print('armando df sku moda por persona')
    g = pd.DataFrame.from_dict(g, orient='index').reset_index().rename(columns={'index':'person',0:'sku'})
    print('preparando df final...')
    df = df.person.drop_duplicates().to_frame().merge(g, on='person', how='outer')
    mode = g.sku.mode()[0]
    df.sku.fillna(mode, inplace=True)
    return df.merge(d, on='sku')
    

# SUBSAMPLE
def undersample(X,y,ratio=.5):
    convIdx = y[y == 1].index.values
    noconvIdx = np.random.choice(y[y == 0].index, size=round(len(X) * ratio))
    final = np.concatenate((convIdx, noconvIdx))
    X1 = X.loc[final]
    y1 = y.loc[final]
    return X1, y1

# JOIN
def joinDFs(dfList):
    return reduce(lambda d1,d2: d1.merge(d2, on='person', how='outer'), dfList)

# PREDICTION
def split(train, labels):
    trainDF = train.merge(labels, on='person')
    trainDF = trainDF.set_index('person')
    X = trainDF[trainDF.columns[:-1]]
    y = trainDF[trainDF.columns[-1]]
    return train_test_split(X, y, test_size=0.15, random_state=0)

def applyTransf(test):
    df1 = lastEvents(test)
    df2 = assoc_rules(test)[['person','ass_rules_lift','ass_rules_confidence']]
    df3 = visitFeatures(test)
    df4 = eventsCount(test)
    df5 = modelPopularity(test)
    return joinDFs([df1,df2,df3,df4,df5])
